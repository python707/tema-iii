# Programa que muestra el uso de la sentencia break que permite
# Terminar un ciclo while infinito

total=''
while True:
    item=input("Proporciona item:")
    if item=="exit":
      break
    total=total+item+'\n'
print(total)
