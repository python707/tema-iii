# Programa que muestra el uso de else de while,
# este se ejecuta cuando la evaluación de while es False
#

total=item=''
while item!='exit':
    item=input("Proporciona item:")
    if item=='exit':
        continue
    total=total+item+'\n'
else:
    total=total+"FIN\n"
print(total)
