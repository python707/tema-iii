# Programa que muestra el uso de bandera para romper un ciclo así
# como el uso de la sentencia continue

total=''
band=True
while band:
    item=input("Proporciona item:")
    if item=="exit":
       band=False
       continue
    total=total+item+'\n'
print(total)
